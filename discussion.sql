-- Creating new record/s
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUE ("Psy-6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUE ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUE ("Gangnam Style", 253, "K-Pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUE ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUE ("Kisapmata", 249, "OPM", 2);

-- Retrieving records from a table
SELECT song_name, genre FROM songs;

-- Retrieve all records from the songs table
SELECT * FROM songs;

-- Retrieve records with a condition
SELECT song_name FROM songs WHERE genre = "OPM";

-- Within the WHERE clause, we can also use the AND and OR operators
SELECT song_name, length FROM songs WHERE length > 230 AND genre = "OPM";


-- Updating an existing record from a table
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- Deleting an existing record from a table
DELETE FROM songs WHERE genre = "OPM" AND length > 240;